import * as React from "react";
import { StatusBar, Button, View, Text, StyleSheet } from "react-native";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import Icon from "react-native-vector-icons/MaterialIcons";

export function Container({ children, title, back, navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        {/* navigation.goBack() */}
        {back && (
          <View style={{ position: "absolute", left: 20 }}>
            <TouchableWithoutFeedback onPress={back}>
              <Icon
                name="arrow-back-ios"
                style={{ color: "white", fontSize: 24 }}
              ></Icon>
            </TouchableWithoutFeedback>
          </View>
        )}
        <Text style={styles.title}>{title}</Text>
      </View>
      {children}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "black",
  },
  header: {
    height: 60,
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    marginTop: 10,
  },
  title: {
    color: "white",
    fontWeight: "bold",
    fontSize: 18,
  },
});
