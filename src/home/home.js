import {
  StatusBar,
  Button,
  View,
  Text,
  FlatList,
  TouchableWithoutFeedback,
  Image,
} from "react-native";
import { Container } from "../Component/Container";
import React, { useState, useEffect } from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import Octicons from "react-native-vector-icons/Octicons";
import { styles } from "./style";

export function HomeScreen({ navigation }) {
  const [movies, setMovies] = useState([]);
  getData = async () => {
    try {
      let response = await fetch(
        "https://api.hkmovie6.com/hkm/movies?type=showing"
      );
      return await response.json();
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    async function fetchData() {
      setMovies(await getData());
    }
    fetchData();
  }, []);

  formatDate = (date) => {
    const d = new Date(date);
    return (
      d.getFullYear() + "年" + (d.getMonth() + 1) + "月" + d.getDay() + "日"
    );
  };

  ratingBar = (rating) => {
    return (
      <View style={{ flexDirection: "row" }}>
        {[0, 1, 2, 3, 4].map((e, i) => {
          if (rating - e > 0 && rating - e < 1) {
            return (
              <View key={i}>
                <Icon name="star" style={styles.grayStar} />
                <Icon
                  name="star-half"
                  style={{ position: "absolute", top: 0, ...styles.yellowStar }}
                />
              </View>
            );
          } else if (rating - e > 0) {
            return <Icon key={i} name="star" style={styles.yellowStar} />;
          } else {
            return <Icon key={i} name="star" style={styles.grayStar} />;
          }
        })}
      </View>
    );
  };

  renderMovies = ({ item, index, separators }) => (
    <TouchableWithoutFeedback
      onPress={() => {
        navigation.navigate("Details", { item });
      }}
    >
      <View style={styles.movieContainer}>
        {/* image */}
        <Image
          style={styles.movieImage}
          source={{ uri: item.thumbnail }}
        ></Image>

        {/* rating */}
        <View style={styles.ratingContainer}>
          {/* There is no rating in the api provided, so I hardcoded one. */}
          <Text style={styles.rating}>3.8</Text>
          {ratingBar(3.8)}
        </View>

        <View style={styles.detailContainer}>
          <Text style={styles.movieTitle}>{item.chiName}</Text>
          <View style={{ flexDirection: "row", marginVertical: 5 }}>
            <Icon
              name="heart-o"
              style={{ marginRight: 8, ...styles.yellowIcon }}
            />
            <Text style={styles.yellowBold}>{item.favCount}</Text>
            <Octicons
              name="comment"
              style={{ marginHorizontal: 8, ...styles.yellowIcon }}
            ></Octicons>
            <Text style={styles.yellowBold}>{item.commentCount}</Text>
          </View>

          <Text style={{ color: "white" }}>{formatDate(item.openDate)}</Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );

  return (
    <Container title="電影">
      <FlatList
        data={movies}
        keyExtractor={(item) => item.id.toString()}
        renderItem={renderMovies}
      />
    </Container>
  );
}
