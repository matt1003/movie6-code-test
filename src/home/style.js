import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  yellowBold: { color: "yellow", fontWeight: "bold" },
  yellowIcon: { color: "white", fontSize: 16 },
  movieContainer: {
    flex: 1,
    flexDirection: "row",
    borderBottomWidth: 0.5,
    borderColor: "gray",
  },
  movieImage: {
    width: 80,
    height: (80 * 1204) / 816,
    backgroundColor: "white",
    resizeMode: "contain",
  },
  ratingContainer: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: 20,
  },
  rating: {
    color: "white",
    fontSize: 45,
    color: "yellow",
    fontWeight: "bold",
  },
  movieTitle: {
    color: "white",
    fontWeight: "bold",
    fontSize: 18,
  },
  detailContainer: {
    flex: 1,
    justifyContent: "center",
    lineHeight: 15,
  },
  yellowStar: { color: "yellow", fontSize: 14 },
  grayStar: { color: "gray", fontSize: 14 },
});
