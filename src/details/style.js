import { StyleSheet } from "react-native";
export const styles = StyleSheet.create({
  yellowIcon: { color: "white", fontSize: 16 },
  yellowBold: { color: "yellow", fontWeight: "bold" },
  movieContainer: {
    flexDirection: "row",
    borderBottomWidth: 0.5,
  },
  movieTitle: {
    color: "white",
    fontWeight: "bold",
    fontSize: 18,
  },
  ratingContainer: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: 20,
  },
  rating: {
    color: "white",
    fontSize: 45,
    color: "yellow",
    fontWeight: "bold",
  },
  movieInfo: { color: "white", paddingLeft: 10, flex: 1 },
  paginationContainer: {
    flexDirection: "row",
    height: 10,
    paddingTop: 0,
    paddingBottom: 0,
  },
  pageDot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 8,
    backgroundColor: "white",
  },
  pageInactiveDot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 8,
    backgroundColor: "gray",
  },
});
