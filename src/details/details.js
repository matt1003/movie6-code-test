import * as React from "react";
import {
  StatusBar,
  Button,
  View,
  Text,
  StyleSheet,
  ScrollView,
  Dimensions,
} from "react-native";
import { useState } from "react/cjs/react.development";
import { Container } from "../Component/Container";
import Icon from "react-native-vector-icons/FontAwesome";
import Octicons from "react-native-vector-icons/Octicons";
import YoutubePlayer from "react-native-youtube-iframe";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { styles } from "./style";

export function DetailsScreen({ route, navigation }) {
  console.log(route.params.item);
  const [detail, setDetail] = useState(route.params.item);
  const [pageIndex, setPageIndex] = useState(0);

  // *** The link from "multitrailers" are not working, but the code also provided

  // const [videos, setVideos] = useState(
  //   detail.multitrailers.map((e) => {
  //     return { link: e, ready: false };
  //   })
  // );

  // so I used some other hardcode links to replace
  const [videos, setVideos] = useState([
    { link: "Af5sAZh3pa0", ready: false },
    { link: "jYqsV2InVJs", ready: false },
    { link: "fdMcUy8-DAo", ready: false },
    { link: "Af5sAZh3pa0", ready: false },
  ]);

  // This api is getting the same data from home page, so I won't use it
  // ​https://api.hkmovie6.com/hkm/movies/{movieId}

  _renderItem = ({ item, index }) => {
    return (
      <View style={{ height: 230 }}>
        {!item.ready && (
          <View style={{ alignItems: "center", padding: 100 }}>
            <Text style={{ color: "white" }}>Loading</Text>
          </View>
        )}
        <YoutubePlayer
          height={230}
          videoId={item.link}
          onReady={() => {
            setVideos(
              videos.map((e, i) => (i == index ? { ...e, ready: true } : e))
            );
          }}
        />
      </View>
    );
  };

  _pagination = () => {
    return (
      <Pagination
        dotsLength={videos.length}
        activeDotIndex={pageIndex}
        containerStyle={styles.paginationContainer}
        dotStyle={styles.pageDot}
        inactiveDotStyle={styles.pageInactiveDot}
      />
    );
  };

  _movieDetail = () => {
    return (
      <View style={styles.movieContainer}>
        {/* rating */}
        <View style={styles.ratingContainer}>
          {/* There is no rating in the api provided, so I hardcoded one. */}
          <Text style={styles.rating}>3.8</Text>
          {ratingBar(3.8)}
        </View>

        {/* movie detail */}
        <View style={{ justifyContent: "center" }}>
          <Text style={styles.movieTitle}>{detail.chiName}</Text>
          <View style={{ flexDirection: "row", marginVertical: 10 }}>
            <Icon
              name="heart-o"
              style={{ marginRight: 8, ...styles.yellowIcon }}
            />
            <Text style={styles.yellowBold}>{detail.favCount}</Text>
            <Octicons
              name="comment"
              style={{ marginHorizontal: 8, ...styles.yellowIcon }}
            ></Octicons>
            <Text style={styles.yellowBold}>{detail.commentCount}</Text>
          </View>

          <View style={{ flexDirection: "row" }}>
            <Text style={{ color: "white" }}>
              {formatDate(detail.openDate)}{" "}
            </Text>
            <Text style={{ color: "white" }}> | </Text>
            <Text style={{ color: "white" }}>122分鐘</Text>
            <Text style={{ color: "white" }}> | </Text>
            <Text style={{ color: "white" }}>{detail.infoDict.Category}</Text>
          </View>
        </View>
      </View>
    );
  };

  _movieInfo = () => (
    <View style={{ paddingHorizontal: 10 }}>
      <Text style={{ color: "white", lineHeight: 18 }}>
        {detail.chiSynopsis}
      </Text>

      {/*  Chinese key @_@....  */}
      <View style={{ flexDirection: "row", marginTop: 30 }}>
        <Text style={{ color: "gray" }}>導演</Text>
        <Text style={styles.movieInfo}>
          {detail.chiInfoDict["導演"] || "---"}
        </Text>
      </View>
      <View style={{ flexDirection: "row", marginTop: 10 }}>
        <Text style={{ color: "gray" }}>演員</Text>
        <Text style={styles.movieInfo}>
          {detail.chiInfoDict["演員"] || "---"}
        </Text>
      </View>
      <View style={{ flexDirection: "row", marginTop: 10 }}>
        <Text style={{ color: "gray" }}>類型</Text>
        <Text style={styles.movieInfo}>
          {detail.chiInfoDict["類型"] || "---"}
        </Text>
      </View>
      <View style={{ flexDirection: "row", marginTop: 10 }}>
        <Text style={{ color: "gray" }}>語言</Text>
        <Text style={styles.movieInfo}>
          {detail.chiInfoDict["語言"] || "---"}
        </Text>
      </View>
    </View>
  );

  return (
    <Container title="電影資料" back={() => navigation.goBack()}>
      <ScrollView>
        <Carousel
          data={videos}
          sliderWidth={Dimensions.get("window").width}
          itemWidth={Dimensions.get("window").width}
          renderItem={_renderItem}
          onSnapToItem={(index) => setPageIndex(index)}
        />
        {_pagination()}
        {_movieDetail()}
        {_movieInfo()}
      </ScrollView>
    </Container>
  );
}
