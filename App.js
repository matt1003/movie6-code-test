// In App.js in a new project

import * as React from "react";
import { StatusBar, Button, View, Text } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import "react-native-gesture-handler";
import { HomeScreen } from "./src/home/home";
import { DetailsScreen } from "./src/details/details";

const Stack = createStackNavigator();

function App({ navigation }) {
  return (
    <NavigationContainer>
      <StatusBar translucent barStyle="light-content" />
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerShown: false,
          // headerTintColor: "white",
          // headerStyle: {
          //   backgroundColor: "black",
          // },
        }}
      >
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Details" component={DetailsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default App;
